import requests

from django.shortcuts import render,redirect
from django.http import HttpResponse

from .userConfig import UserConfig


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def getcredentials(request):
    userConf = UserConfig()
    

def overview(request):
    userConf = UserConfig()
    cred_token = request.GET.get("code",None)
    userConf.access_token =  userConf.get_cred_token(cred_token)
    
    return HttpResponse(userConf.get_profile())

def userprofile(request):
    userConf = UserConfig()
    cred_token = request.GET.get("code",None)
    userConf.access_token =  userConf.get_cred_token(cred_token)
    
    return HttpResponse(userConf.get_profile())


def previoustracks(request):
    userConf = UserConfig()
    cred_token = request.GET.get("code",None)
    userConf.access_token =  userConf.get_cred_token(cred_token,"previoustracks")
    track_list = userConf.get_users_tracks()
    username = userConf.get_profile()
    title = "Recently played artists :"


    context = {"track_list" : track_list, "username" : username,"title":title}
    
    return render(request, 'apiPoopify/tracklist.html', context)

def favtracks(request):
    userConf = UserConfig()
    cred_token = request.GET.get("code",None)
    userConf.access_token =  userConf.get_cred_token(cred_token,"favtracks")
    track_list = userConf.get_user_favorites()
    username = userConf.get_profile()
    title = "The artists you liked the most those previous months :"

    context = {"track_list" : track_list, "username" : username,"title":title}
    
    return render(request, 'apiPoopify/tracklist.html', context)

def getuserfromform(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        playlist = request.POST.get('playlist',None)
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'apiPoopify/getuserplaylist.html', {
            'error_message': "You didn't select a choice.",
        })
    else:
        context = {"track_list" : playlist}
        return render(request, 'apiPoopify/tracklist.html', context)

def userplaylist(request):
    userConf = UserConfig()
    cred_token = request.GET.get("code",None)
    userConf.access_token =  userConf.get_cred_token(cred_token,"playlist")
    track_list = userConf.get_user_playlist("impyamin")
    username = userConf.get_profile()
    title = "User's playlist :"

    context = {"track_list" : track_list, "username" : username,"title":title}
    
    return render(request, 'apiPoopify/tracklist.html', context)

def getfavtracks(requests):
    userConf = UserConfig()
    return redirect(userConf.get_credentials("user-top-read","http://127.0.0.1:8000/apiPoopify/favtracks"),'apiPoopify/tracklist.html')

def getprevioustracks(requests):
    userConf = UserConfig()
    return redirect(userConf.get_credentials("user-read-recently-played","http://127.0.0.1:8000/apiPoopify/previoustracks"),'apiPoopify/tracklist.html')

def getuserplaylist(requests):
    userConf = UserConfig()
    return redirect(userConf.get_credentials("playlist-read-private","http://127.0.0.1:8000/apiPoopify/getuserplaylist"),'apiPoopify/tracklist.html')





