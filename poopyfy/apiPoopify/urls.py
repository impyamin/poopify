from django.urls import path

from . import views

urlpatterns = [
    path('index/', views.index, name='index'),
    path('getcredentials', views.getcredentials, name='getcredentials'),
    path('userprofile', views.userprofile, name='userprofile'),
    path('previoustracks', views.previoustracks, name='previoustracks'),
    path('favtracks', views.favtracks, name='favtracks'),
    path('getfavtracks', views.getfavtracks, name='getfavtracks'),
    path('getprevioustracks', views.getprevioustracks, name='getprevioustracks'),
    path('getuserplaylist', views.getuserplaylist, name='getuserplaylist'),
    path('userplaylist', views.userplaylist, name='userplaylist'),
    path('', views.overview, name='overview'),
]