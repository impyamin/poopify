import requests

class UserConfig():

    client_secret = None
    client_id = None
    token = None
    code_token = None
    access_token = None

    def __init__(self):
        self.client_secret = '88f6895f9066488ba19455dc58b8868c'
        self.client_id = 'eac5efe4d20a47828d08d6b04ba158ac'
     

    def get_new_token(self, cli_id, cli_sec):
        url='https://accounts.spotify.com/api/token'
        grant_type = 'client_credentials'
        body_params = {'grant_type' : grant_type}
        response = requests.post(url, data=body_params, auth = (self.client_id, self.client_secret)) 
        self.token = response.json()['access_token']
        return self.token

    def get_new_releases(self):
        self.get_new_token( "", "")
        url='https://api.spotify.com/v1/browse/new-releases'
        headers = {       
        'Authorization': 'Bearer {}'.format(self.token),        
        }
        body_params = {'country' : 'FR',
                       'limit': 10,
                       'offset':5 
                       }

        response = requests.post(url,headers=headers, params=body_params, auth = (self.client_id, self.client_secret)) 
        return response
    
    def get_credentials(self,scope,redir_url):
        self.get_new_token("", "")
        url='https://accounts.spotify.com/authorize'

        url = url + "?client_id=" + self.client_id + '&response_type=code&redirect_uri='+redir_url + "&scope="+scope
        
        return url
        

    def get_profile(self):
        self.get_new_token( "", "")
        url="https://api.spotify.com/v1/users/impyamin/playlists"
        data = {
                "name":"title",
                "public":"false"
        }
        headers = { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + self.access_token
        }      

        response = requests.get(url,headers=headers).json()
        username = response["items"][0]["owner"]["display_name"]
        return username
    
    def get_users_tracks(self):
        self.get_new_token( "", "")
        url="https://api.spotify.com/v1/me/player/recently-played?type=track&limit=10"
        headers = { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + self.access_token,
        }          

        response = requests.get(url,headers=headers).json()
        tracklist = response["items"]

        links = []
        recent_artists = []
        for item in tracklist : 
            try :
                if item["track"] != "" :
                    recent_artists.append(item["track"]["album"]["artists"][0]["name"])  
                    links.append(item["track"]["album"]["artists"]
                                              [0]["external_urls"]["spotify"])           

                             
            except KeyError :
                recent_artists.append("error")
        return recent_artists
    
    def get_user_playlist(self,username):
        self.get_new_token( "", "")
        url="https://api.spotify.com/v1/users/"+username+"/playlists?limit=10&offset=5"
        headers = { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + self.access_token,
        }          

        response = requests.get(url,headers=headers).json()
        tracklist = response

        links = []
        for item in tracklist : 
            try :
                if item != "" :
                    playlist.append(item)
                               

                             
            except KeyError :
                recent_artists.append("error")
        return recent_artists
    def get_user_favorites(self):
        self.get_new_token( "", "")
        url="https://api.spotify.com/v1/me/top/artists?time_range=medium_term&limit=10&offset=5"
        headers = { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + self.access_token,
        }          

        response = requests.get(url,headers=headers).json()
        tracklist = response["items"]

        links = []
        fav_artists = []
        for item in tracklist : 
            try :
                if item != "" :
                    fav_artists.append(item["name"])                                                           
            except KeyError :
                fav_artists.append("error")
        return fav_artists
    
    def get_cred_token(self,code, ret_url):
        self.code_token = code
        url = "https://accounts.spotify.com/api/token"

        headers = {
        "grant_type":    "authorization_code",
        "code":          self.code_token,
        "redirect_uri":  "http://127.0.0.1:8000/apiPoopify/"+ret_url,
        "client_secret": self.client_secret,
        "client_id":     self.client_id,
        }

        # headers = {       
        # 'Authorization': 'Bearer {}'.format(self.token),        
        # }      

        response = requests.post(url,data=headers)
        self.access_token = response.json()["access_token"]
        return self.access_token